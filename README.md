# Prometheus daily values lambda function

Python code for an AWS Lambda function to read values from a particular remote Prometheus and write them to an AWS timestream database.

The repository includes CI/CD-scripts to package the code into a zip file and upload this file to AWS S3.

## Deployment

* Provide GitLab with the variables required in [.gitlab-ci.yaml](./.gitlab-ci.yaml)
* Run GitLab CI to upload the zip file to AWS S3
* Run the zip file in a lambda function, providing that function with the variables listed in [lambda_environment.env](./lambda_environment.env)

## Development

* create a virtual environment (`mkvirtualenv prometheus-daily-values`)
* activate virtual environment (`workon prometheus-daily-values`)
* install requirements (`pip install -r requirements.txt`)
* install development requirements (`pip install -r requirements_local.txt`)
* install pre-commit hooks (`pre-commit install`)

## Testing

After installing the development requirements, use [python-lambda-local](https://pypi.org/project/python-lambda-local/) to run the lambda function locally.

You can create local .json-files to serve as payload, for example.

Please note: the code has no actual 'test' mode.
If you run the code locally with a timestream instance configured, it will write to that database just as the lambda would.
