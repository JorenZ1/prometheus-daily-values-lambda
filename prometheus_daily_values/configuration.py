import inspect
import logging
import os
import traceback
from typing import List

import boto3
import sys
from botocore.config import Config as BotoConfig
from pythonjsonlogger import jsonlogger


class ConfigurationError(Exception):
    pass


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def __init__(self, *args, **kwargs):
        self.aws_request_id = kwargs.pop("aws_request_id")
        super().__init__(*args, **kwargs)

    def add_fields(self, log_record, record, message_dict):
        super().add_fields(log_record, record, message_dict)
        log_record["aws_request_id"] = self.aws_request_id
        if log_record.get("level"):
            log_record["level"] = log_record["level"].upper()
        else:
            log_record["level"] = record.levelname


def get_exception_type_and_traceback() -> (str, List[str]):
    exception_type, exception_value, exception_traceback = sys.exc_info()
    stack = traceback.extract_tb(exception_traceback)
    return str(exception_type.__name__), traceback.format_list(stack)


class Config:
    def __init__(self, event, aws_request_id: str):
        self.log_level = logging.getLevelName(event.get("log_level", logging.INFO))
        self.logger = None
        self.query_time_range: str = "24h"
        self.prometheus_url: str = os.getenv("PROMETHEUS_DAILY_VALUES_PROMETHEUS_URL")
        self.prometheus_api_user: str = os.getenv(
            "PROMETHEUS_DAILY_VALUES_PROMETHEUS_API_USER"
        )
        self.prometheus_api_secret: str = os.getenv(
            "PROMETHEUS_DAILY_VALUES_PROMETHEUS_API_KEY"
        )
        self.aws_region_name = os.getenv("PROMETHEUS_DAILY_VALUES_AWS_REGION_NAME")
        self.timestream_batch_size: int = 100
        self.timestream_database: str = os.getenv(
            "PROMETHEUS_DAILY_VALUES_TIMESTREAM_DATABASE_NAME"
        )
        self.timestream_table: str = os.getenv(
            "PROMETHEUS_DAILY_VALUES_TIMESTREAM_TABLE_NAME"
        )

        self.check_config()
        self.set_logger(aws_request_id)
        self.set_timestream_client()

    def check_config(self):
        for key, value in inspect.getmembers(self):
            if value is None:
                raise ConfigurationError(
                    f"ensure configuration property '${key}' can be defined from the environment and/or the event"
                )

    def set_logger(self, aws_request_id: str):
        formatter = CustomJsonFormatter(aws_request_id=aws_request_id)

        handler = logging.StreamHandler(sys.stderr)
        handler.setFormatter(formatter)
        handler.setLevel(self.log_level)

        self.logger = logging.getLogger()
        self.logger.setLevel(self.log_level)

        # Fail if anything other than the single expected Lambda default handler is present
        assert len(self.logger.handlers) == 1
        self.logger.removeHandler(self.logger.handlers[0])
        self.logger.addHandler(handler)

    def set_timestream_client(self):
        session = boto3.Session(region_name=self.aws_region_name)
        self.timestream_client = session.client(
            "timestream-write",
            config=BotoConfig(
                read_timeout=20, max_pool_connections=5000, retries={"max_attempts": 10}
            ),
        )
