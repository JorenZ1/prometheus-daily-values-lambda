from typing import Dict, List, NamedTuple


class TimestreamRecord(NamedTuple):
    MeasureName: str
    MeasureValue: str
    Time: str
    Version: int
    Dimensions: List[Dict[str, str]] = [
        {
            "Name": "origin",
            "Value": "prometheus",
            "DimensionValueType": "VARCHAR",
        }
    ]
    MeasureValueType: str = "DOUBLE"
    TimeUnit: str = "MILLISECONDS"
