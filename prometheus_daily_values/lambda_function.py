import json
from typing import List

import requests
from botocore.errorfactory import ClientError
from math import ceil

from configuration import Config, get_exception_type_and_traceback
from model import TimestreamRecord


def get_single_series_from_prometheus_api(
    config: Config, series_name: str, query: str
) -> List[TimestreamRecord]:
    """
    Calls the Prometheus API with the given query, and returns the first series of metrics from the result.
    It converts the metrics into TimestreamRecords with the given series_name as MeasureName.
    :param config:
    :param series_name: the name of the series, e.g. "gas"
    :param query: the query to perform against prometheus, e.g. 'my_metric{tag="orange"} [4h]'
    :return: a list of TimestreamRecords
    """
    response = requests.post(
        url=f"{config.prometheus_url}/api/v1/query",
        auth=(config.prometheus_api_user, config.prometheus_api_secret),
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data={"query": query},
    )
    if response.status_code != 200:
        config.logger.error(
            {
                "message": "get single series from Prometheus API",
                "response_request": response.request.url,
                "expected_status_code": 200,
                "actual_status_code": response.status_code,
            }
        )
        raise requests.exceptions.HTTPError

    prometheus_response = json.loads(response.content)
    result = []
    for value_pair in prometheus_response["data"]["result"][0]["values"]:
        config.logger.debug(f"parsing {series_name} value pair {value_pair}")
        time = float(value_pair[0])
        value = value_pair[1]

        timestream_record: TimestreamRecord = TimestreamRecord(
            MeasureName=series_name,
            MeasureValue=value,
            Time=str(int(time * 1000)),
            Version=int(time * 1000),
        )
        result.append(timestream_record)

    config.logger.debug(
        {
            "message": "get single series from Prometheus API",
            "num_results": len(result),
            "sample_results": result[0 : min(len(result), 5)],
        }
    )

    return result


def write_records_to_timestream(config: Config, records: List[TimestreamRecord]) -> None:
    """
    Writes the given records to Timestream, using the writer in the given config.
    Takes care of batching the writes per 100.
    :param config:
    :param records:
    :return:
    """
    num_batches = len(records) / config.timestream_batch_size
    num_batches = ceil(num_batches)
    chunks = [
        records[i * config.timestream_batch_size : (i + 1) * config.timestream_batch_size]
        for i in range(0, num_batches)
    ]
    config.logger.debug(
        {
            "message": "write records to timestream",
            "num_records": len(records),
            "num_batches": num_batches,
        }
    )

    for chunk in chunks:
        try:
            config.timestream_client.write_records(
                DatabaseName=config.timestream_database,
                TableName=config.timestream_table,
                CommonAttributes={},
                Records=[value._asdict() for value in chunk],
            )
        except ClientError as client_error:
            exception_type, traceback = get_exception_type_and_traceback()
            config.logger.error(
                {
                    "message": "writing records to timestream failed",
                    "client_error": client_error,
                    "exception_type": exception_type,
                    "traceback": traceback,
                }
            )
            raise client_error


def lambda_handler(event, context):
    config = Config(event, context.aws_request_id)
    config.logger.info(
        {
            "message": "Prometheus daily values",
        }
    )

    try:
        gas_values = get_single_series_from_prometheus_api(
            config=config,
            series_name="gas_cubic_meters",
            query=f"smartmeter_gas_cubicmeters_total [{config.query_time_range}]",
        )
        electricity_high_values = get_single_series_from_prometheus_api(
            config=config,
            series_name="electricity_high_kilowatthours",
            query=f'smartmeter_power_kilowatthours_total{{type="in", rate="high"}} [{config.query_time_range}]',
        )
        electricity_low_values = get_single_series_from_prometheus_api(
            config=config,
            series_name="electricity_low_kilowatthours",
            query=f'smartmeter_power_kilowatthours_total{{type="in", rate="low"}} [{config.query_time_range}]',
        )
        config.logger.debug(
            {
                "message": "retrieve values",
                "num_gas_values": len(gas_values),
                "num_electricity_high_values": len(electricity_high_values),
                "num_electricity_low_values": len(electricity_low_values),
            }
        )
        write_records_to_timestream(
            config, gas_values + electricity_high_values + electricity_low_values
        )
        config.logger.info(
            {
                "message": "wrote Prometheus values to Timestream",
                "event": event,
                "num_gas_values": len(gas_values),
                "num_electricity_high_values": len(electricity_high_values),
                "num_electricity_low_values": len(electricity_low_values),
            }
        )
        return {"statusCode": 200, "body": "OK"}
    except Exception:
        return {"statusCode": 500, "body": "error writing records"}
